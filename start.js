/** Start the web server.
|*|
|*| Author: Ryan Moeller
|*|
|*| This script starts the web server.
|*|
|*| If the dependencies have not been built, they are automatically fetched and
|*| compiled for the local system. A working build system is expected.
|*|
|*| All activity happens in the work directory:
|*|  • Archives are downloaded there.
|*|  • Packages install there.
|*|  • Nothing changes outside of there.
**/

"use strict";

const child_process = require('child_process');
const http = require('http');
const path = require('path');
const fs = require('fs');
const os = require('os');

// Where to work from
const work_dir = path.join(__dirname, 'work');
const src_dir = path.join(__dirname, 'src');

// What Racket module to run
const server_module = 'server';

// Racket archive configuration
const rkt_version = "6.9";
const rkt_archive_server = "http://download.racket-lang.org";

const rkt_archive_path = path.join("releases", rkt_version, "installers");
const rkt_archive = "racket-minimal-" + rkt_version + "-src-builtpkgs.tgz";
const rkt_archive_url = path.join(rkt_archive_server,
                                  rkt_archive_path,
                                  rkt_archive);

const rkt_path = path.join(work_dir, "racket-" + rkt_version);
const rkt_src = path.join(rkt_path, "src");
const rkt_bin = path.join(rkt_path, "bin");
const rkt_cmd = path.join(rkt_bin, "racket");

const rkt_libs = [ 'web-server-lib',
                   'db-lib'
                 ];

// Misc child_process helpers
const exit_success = 0;
const inherit_stdio = {
    stdio: [ 'inherit', 'inherit', 'inherit' ]
};
const ncpus = os.cpus().length;

const make_cmd = ['freebsd', 'openbsd'].includes(os.platform()) ?
      'gmake' : 'make';

function start() {
    check_dir_exists(work_dir, (exists) => {
        if (exists) {
            process.chdir(work_dir);
            spawn_server();
        } else {
            prepare_work_dir();
        }
    });
}

function spawn_server() {
    const module_file = server_module + '.rkt';
    const module_path = path.join(src_dir, module_file);
    const cmd = rkt_cmd;
    const args = [ module_path // The file to run
                 ];
    child_process.spawn(cmd, args, inherit_stdio)
        .on('exit',
            (code, signal) => console.log(code == exit_success ?
                                          "Success." : "Server died."));
}

// Generic helper
function check_dir_exists(dir, callback) {
    fs.stat(dir, (err, stats) => callback(err ? false : true));
}

function prepare_work_dir() {
    fs.mkdir(work_dir, (err, dir) => {
        if (err) {
            console.log(err);
            console.log("check permissions of " + __dirname);
        }
        else {
            process.chdir(work_dir);
            fetch_racket_archive();
        }
    });
}

function fetch_racket_archive() {
    const cmd = 'curl';
    const args = [ '-L', // Follow redirects
                   '-O', // Use the filename from the URL
                   rkt_archive_url
                 ];
    child_process.spawn(cmd, args, inherit_stdio)
        .on('exit', (code, signal) => {
            if (code == exit_success) {
                extract_racket_archive();
            }
            else {
                console.log("Failed to fetch Racket distribution.");
            }
        });
 }

function extract_racket_archive() {
    const cmd = 'tar';
    const args = [ 'xf', rkt_archive ]; // Extract file
    child_process.spawn(cmd, args, inherit_stdio)
        .on('exit', (code, signal) => {
            if (code == exit_success) {
                process.chdir(rkt_src);
                configure_racket();
            }
            else {
                console.log("Failed to extract Racket distribution.");
            }
        });
}

function configure_racket() {
    const cmd = './configure';
    const args = [];
    child_process.spawn(cmd, args, inherit_stdio)
        .on('exit', (code, signal) => {
            if (code == exit_success) {
                make_racket();
            }
            else {
                console.log("Failed to configure Racket distribution.");
            }
        });
}

function make_racket() {
    const cmd = make_cmd;
    const args = [ '-j', ncpus ]; // Use multiple cores
    child_process.spawn(cmd, args, inherit_stdio)
        .on('exit', (code, signal) => {
            if (code == exit_success) {
                install_racket();
            }
            else {
                console.log("Failed to make Racket distribution.");
            }
        });
}

function install_racket() {
    const cmd = make_cmd;
    const args = [ 'install' ];
    child_process.spawn(cmd, args, inherit_stdio)
        .on('exit', (code, signal) => {
            if (code == exit_success) {
                process.chdir(work_dir);
                install_racket_libs();
            }
            else {
                console.log("Failed to install Racket distribution.");
            }
        });
}

function install_racket_libs() {
    const cmd = path.join(rkt_bin, 'raco');
    const args = [ 'pkg', 'install',
                   '--auto', // Don't ask for confirmation
                   '--scope', 'installation', // Install libs to the work dir
                   '-j', ncpus, // Use multiple cores
                 ].concat(rkt_libs);
    child_process.spawn(cmd, args, inherit_stdio)
        .on('exit', (code, signal) => {
            if (code == exit_success) {
                spawn_server();
            }
            else {
                console.log("Failed to install Racket libraries.");
            }
        });
}

start();
