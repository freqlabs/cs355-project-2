## Configuration

Copy the file `src/db-connection-sample.rkt` to `src/db-connection.rkt` and fill
in the connection parameters.

## Starting the server

```sh
npm start
```
