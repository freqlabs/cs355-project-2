#lang racket/base
(require db)
(provide db-connection)

(define db-connection
  (virtual-connection
   (connection-pool
    (lambda ()
      (mysql-connect #:user "username"
                     #:password "password"
                     #:database "database"
                     #:server "hostname")))))
