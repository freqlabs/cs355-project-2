#lang racket/base
(require racket/list
         racket/match
         racket/function
         web-server/http
         web-server/dispatch
         net/url-structs
         json
         "model.rkt")
(provide api-dispatch)

(define (api-dispatch req . path)
  (match path
    [`(,(cons "v1.0" rest)) ((api-v1.0 rest)   req)]
    [`(,(cons "v1" rest))   ((api-v1 rest)     req)]
    [`(,rest)               ((api-latest rest) req)]
    [_ not-found]))

;;
;; API Version 1.0
;;

(define (api-v1.0 path)
  (define (valid-entity? name)
    (define valid-entities '("users" "aliases" "domains" "records"))
    (not (eq? #f (member name valid-entities))))
  (match path
    [(cons name rest) #:when (valid-entity? name) (api-v1.0-entity name rest)]
    [_ not-found]))

(define (api-v1.0-entity name path)
  (define collection-methods #"POST, GET, OPTIONS")
  (define entities
    (lambda (req)
      (case (string-downcase (bytes->string/utf-8 (request-method req)))
        [("post") (create-entity req)]
        [("get" ) (read-entities req)]
        [("head" "options") (options collection-methods)]
        [else response-405])))

  (define entity-methods #"GET, PUT, PATCH, DELETE, OPTIONS")
  (define (entity id)
    (lambda (req)
      (case (string-downcase (bytes->string/utf-8 (request-method req)))
        [("post"  ) (post-not-allowed   id)]
        [("get"   ) (read-entity    req id)]
        [("put"   ) (replace-entity req id)]
        [("patch" ) (modify-entity  req id)]
        [("delete") (delete-entity  req id)]
        [("head" "options") (options entity-methods)]
        [else response-405])))

  (define (post-not-allowed id)
    (if (entity-exists? id) response-409 response-405))

  (define entity-exists? (curry model/exists? name))

  (define (model-action action methods)
    (compose1
     (curry json-response 200 #"Okay" (headers methods))
     list
     jsexpr->bytes
     (lambda (x)
       (cond
        [(number? x) x]
        [(list? x) (map vector->list x)]
        [(vector? x) (vector->list x)]))
     (curry action name)))
  (define (model-void-action action methods)
    (compose1
     (curry json-response 204 #"No Content" (headers methods))
     (lambda (x) empty)
     (curry action name)))
  (define entity-void-action (curryr model-void-action entity-methods))

  (define create-entity
    (compose1
     (lambda (id)
       (json-response
        201 #"Created"
        (cons (header #"Location"
                      (string->bytes/utf-8
                       (string-append "/api/" name "/" id)))
              (headers collection-methods))
        (list (jsexpr->bytes id))))
     number->string
     (curry model/create name)))
  (define read-entity    (model-action model/read     entity-methods    ))
  (define read-entities  (model-action model/read-all collection-methods))
  (define replace-entity (entity-void-action model/replace))
  (define modify-entity  (entity-void-action model/modify ))
  (define delete-entity  (entity-void-action model/delete ))

  (match path
    ['() entities]
    [`(,id) #:when (not (equal? "" id)) (entity id)]
    [_ not-found]))

(define api-v1     api-v1.0)
(define api-latest api-v1  )

(define (json-response code desc headers body)
  (response/full code desc
                 (current-seconds) #"application/json"
                 headers body))

(define (error-response code desc)
  (json-response code desc empty (list desc)))

(define response-404 (error-response 404 #"Not found"))
(define response-405 (error-response 405 #"Method not allowed"))
(define response-409 (error-response 409 #"Conflict"))

(define (not-found   req) response-404)

(define (headers allowed)
  `(,(header #"Allow" #"*")
    ,(header #"Content-Type" #"application/json")
    ,(header #"Access-Control-Allow-Origin" #"*")
    ,(header #"Access-Control-Allow-Headers" #"Content-Type")
    ,(header #"Access-Control-Allow-Methods" allowed)))

(define (options allowed)
  (json-response 200 #"Okay" (headers allowed) empty))
