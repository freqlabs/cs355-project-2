#lang racket
(require racket/list
         racket/function
         web-server/http
         web-server/templates
         "nav-item.rkt")
(provide page-template
         top-nav-items)

(define (page-template tabbed page-title page-content top-nav-items side-nav-items)
  (response/full
   200 #"Okay"
   (current-seconds) TEXT/HTML-MIME-TYPE
   empty
   (list
    (string->bytes/utf-8
     (let ([project "Server Admin Portal"]
           [description "server admin portal dns email dashboard"])
       (include-template "../views/page-template.html"))))))

(define (top-nav-items active)
  (let ([item (curry make-nav-item active)])
    (list (item "/"       "Dashboard")
          (item "/domain" "Domain")
          (item "/email"  "Email")
          (item "/about"  "About"))))
