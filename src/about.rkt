#lang racket/base
(require racket/function
         web-server/templates
         "page-template.rkt"
         "nav-item.rkt")
(provide about-page)

(define (about-page req)
  (define side-nav
    (let ([item (curry make-nav-item "Purpose")])
      (list (item "#purpose"    "Purpose")
            (item "#developer"  "About the Developer")
            (item "#data-model" "Data Model"))))
  (let* ([tabbed #f]
         [page-title "About"]
         [top-nav (top-nav-items page-title)]
         [page-content (include-template "../views/about.html")])
    (page-template tabbed page-title page-content top-nav side-nav)))
