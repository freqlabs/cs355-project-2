#lang racket/base
(require racket/function
         web-server/templates
         "page-template.rkt"
         "nav-item.rkt"
         "model.rkt")
(provide email-page)

(define (email-page req)
  (define side-nav
    (let ([item (curry make-nav-item "Overview")])
      (list (item "#overview" "Overview")
            (item "#accounts" "Accounts")
            (item "#aliases"  "Aliases"))))
  (let* ([tabbed #t]
         [page-title "Email"]
         [top-nav (top-nav-items page-title)]
         [accounts (all-accounts)]
         [alias-counts (top-alias-counts)]
         [domains (all-domains)]
         [page-content (include-template "../views/email.html")])
    (page-template tabbed page-title page-content top-nav side-nav)))
