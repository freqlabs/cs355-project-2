#lang racket/base
(require racket/function
         web-server/templates
         "page-template.rkt"
         "nav-item.rkt"
         "model.rkt")
(provide domain-page)

(define (domain-page req)
  (define side-nav
    (let ([item (curry make-nav-item "Overview")])
      (list (item "#overview" "Overview")
            (item "#domains"  "Domains")
            (item "#records"  "Records"))))
  (let* ([tabbed #t]
         [page-title "Domain"]
         [top-nav (top-nav-items page-title)]
         [domains-without-records (recordless-domains)]
         [domains (all-domains)]
         [page-content (include-template "../views/domain.html")])
    (page-template tabbed page-title page-content top-nav side-nav)))
